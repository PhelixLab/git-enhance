## Commit-msg
- Check my commit format in every commit.
## Pre-push
- Check my commits when push on master.
- Commits should have fix/test/refactor prefix.
## Post-commit
- Auto tag when pubspec file has new version.
